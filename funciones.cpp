// Para poder usar string como se usa en estas funciones es necesario incluir iostream.
// Tambien es necesario setear el namespace como std para no usar std::string
#include <iostream>
using namespace std;

void hello_world () {
  printf("Hello, world!\n");
}

string s_hello_world () {
  return "Hello, world!\n";
} 
